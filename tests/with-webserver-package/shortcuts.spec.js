const express = require('express');
const WebThingHTTPTester = require('webthing-http-tester');

describe('when the main path is root', () => {
  test('getActions', (done) => {
    const app = express();
    app.get('/', function (_, res) {
      res.json({
        links: [
          {
            'rel': 'actions',
            'href': '/actions2',
          },
        ],
      });
    });
    app.get('/actions2', function (_, res) {
      res.json({correct: true});
    });
    const server = app.listen(0, async() => {
      const port = server.address().port;
      const tester = new WebThingHTTPTester({port});
      expect(await tester.getActions()).toEqual({correct: true});
      server.close();
      done();
    });
  });

  test('getEvents', (done) => {
    const app = express();
    app.get('/', function (_, res) {
      res.json({
        links: [
          {
            'rel': 'events',
            'href': '/things/pi/evts',
          },
        ],
      });
    });
    app.get('/things/pi/evts', function (_, res) {
      res.json({correct: true});
    });
    const server = app.listen(0, async() => {
      const port = server.address().port;
      const tester = new WebThingHTTPTester({port});
      expect(await tester.getEvents()).toEqual({correct: true});
      server.close();
      done();
    });
  });

  test('getProperties', (done) => {
    const app = express();
    app.get('/', function (_, res) {
      res.json({
        links: [
          {
            'rel': 'properties',
            'href': '/properties',
          },
        ],
      });
    });
    app.get('/properties', function (_, res) {
      res.json({correct: true});
    });
    const server = app.listen(0, async() => {
      const port = server.address().port;
      const tester = new WebThingHTTPTester({port});
      expect(await tester.getProperties()).toEqual({correct: true});
      server.close();
      done();
    });
  });
});

describe('when a main path was specified', () => {
  test('getActions', (done) => {
    const app = express();
    app.get('/thing/light', function (_, res) {
      res.json({
        links: [
          {
            'rel': 'actions',
            'href': '/actions2',
          },
        ],
      });
    });
    app.get('/actions2', function (_, res) {
      res.json({correct: true});
    });
    const server = app.listen(0, async() => {
      const port = server.address().port;
      const tester = new WebThingHTTPTester({port, path: '/thing/light'});
      expect(await tester.getActions()).toEqual({correct: true});
      server.close();
      done();
    });
  });

  test('getEvents', (done) => {
    const app = express();
    app.get('/thing/light', function (_, res) {
      res.json({
        links: [
          {
            'rel': 'events',
            'href': '/things/pi/evts',
          },
        ],
      });
    });
    app.get('/things/pi/evts', function (_, res) {
      res.json({correct: true});
    });
    const server = app.listen(0, async() => {
      const port = server.address().port;
      const tester = new WebThingHTTPTester({port, path: '/thing/light'});
      expect(await tester.getEvents()).toEqual({correct: true});
      server.close();
      done();
    });
  });

  test('getProperties', (done) => {
    const app = express();
    app.get('/thing/light', function (_, res) {
      res.json({
        links: [
          {
            'rel': 'properties',
            'href': '/properties',
          },
        ],
      });
    });
    app.get('/properties', function (_, res) {
      res.json({correct: true});
    });
    const server = app.listen(0, async() => {
      const port = server.address().port;
      const tester = new WebThingHTTPTester({port, path: '/thing/light'});
      expect(await tester.getProperties()).toEqual({correct: true});
      server.close();
      done();
    });
  });
});
