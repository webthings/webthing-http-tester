const express = require('express');
const WebThingHTTPTester = require('webthing-http-tester');

describe('getLinks', () => {
  const app = express();
  let server, tester;
  app.get('/', function (_, res) {
    res.json({
      links: [
        {
          'rel': 'actions',
          'href': '/actions2',
        },
      ],
    });
  });
  beforeAll(() => {
    return new Promise((resolve) => {
      server = app.listen(0, () => {
        const port = server.address().port;
        tester = new WebThingHTTPTester({port});
        resolve();
      });
    });
  });
  afterAll(() => {
    server.close();
  });
  test('returns correct object on success', async() => {
    expect(await tester.getLink('actions')).toEqual({
      'rel': 'actions',
      'href': '/actions2',
    });
  });
  test('rejects on non-existent rel', async() => {
    await expect(tester.getLink('invalid')).rejects.toThrow('Web Thing has no invalid link.');
  });
});
