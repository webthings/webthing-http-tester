const http = require('http');

const defaultOptions = {
  host: 'localhost',
  port: 80,
  path: '/',
};

function findLinkByRel(links, rel) {
  return links.find(link => (
    link.rel === rel
  ));
}

class WebThingHTTPTester {
  constructor(options) {
    this.options = {
      ...defaultOptions,
      ...options,
    };
    if (!this.options.path.endsWith('/')) {
      this.options.path += '/';
    }
  }

  init() {
    return Promise.resolve();
  }

  async getActions() {
    const link = await this.getLink('actions');
    return this.getResponse(link.href);
  }

  async getEvents() {
    const link = await this.getLink('events');
    return this.getResponse(link.href);
  }

  async getLink(rel) {
    const data = await this.getResponse('');
    if (!data.links) {
      throw new Error(`Web Thing does not propagate links.`);
    }
    const link = findLinkByRel(data.links, rel);
    if (!link) {
      throw new Error(`Web Thing has no ${rel} link.`);
    }
    return link;
  }

  async getProperties() {
    const link = await this.getLink('properties');
    return this.getResponse(link.href);
  }

  getResponse(path) {
    if (typeof path !== 'string') {
      throw new Error(`Path must be a string.`);
    }
    if (!path.startsWith('/')) {
      path = this.options.path + path;
    }
    return new Promise((resolve, reject) => {
      const url = `http://${this.options.host}:${this.options.port}${path}`;
      http.get(url, (res) => {
        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
          resolve(JSON.parse(rawData));
        });
      }).on('error', reject);
    });
  }

  async getProperty(name) {
    return await this.getProperties().then((properties) => (
      properties[name]
    ));
  }

  setProperty(name, value) {
    const postData = JSON.stringify({
      [name]: value,
    });
    const options = {
      hostname: this.options.host,
      port: this.options.port,
      path: `/properties/${name}`,
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(postData),
      },
    };
    return new Promise((resolve, reject) => {
      const req = http.request(options, (res) => {
        if (res.statusCode === 400) {
          const message = 'Server answered HTTP error 400: BAD REQUEST\n' +
            'You should report this as a bug.';
          reject(message);
          return;
        }
        if (res.statusCode !== 200) {
          //console.log(`STATUS: ${res.statusCode}`);
          //console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
          reject(`Server answered with status code ${res.statusCode}.`);
          return;
        }
        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
          resolve(JSON.parse(rawData));
        });
      }).on('error', reject);
      // write data to request body
      req.write(postData);
      req.end();
    });
  }
}

module.exports = WebThingHTTPTester;
