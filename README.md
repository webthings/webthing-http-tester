# Web Thing HTTP Tester

This package helps to test any HTTP [Web Thing](https://iot.mozilla.org/wot/) in Node.js.

## Installation
Download the repository into a folder and install the dependencies. You may do this by running:

```
npm install --save-dev webthing-http-tester
```

or if you prefer yarn

```
yarn add --dev webthing-http-tester
```

## Use
Load and initialize a tester per WebThing:
```
const WebThingHTTPTester = require('webthing-http-tester');
const tester = new WebThingHTTPTester({
  port: 8000
});
```

A WebThingHTTPTester has the following parameters:
* **host** (default: "localhost")
* **port** (default: 80)
* **path** (default: "/")

You may request any absolute URL for the host and port, by calling `getResponse`. Like all methods of a tester, this function will return a promise which will get resolved with a result or rejected with an error. Network requests have no specified timeout.

```
await tester.getResponse('/properties/on');
```

### Links
For fast access to links use the as asynchronous function `getLink`. The function will reject its promise, if no such links exists.

```javascript
await tester.getLink('properties');
await tester.getLink('events');
await tester.getLink('actions');
await tester.getLink('alternative');
```

The promise will resolve an object like this:

```json
{
  "rel": "actions",
  "href": "/actions",
  "mediaType": "application/json"
}
``` 

### Properties
To request the value of all properties, use the asynchronous function `getProperties`.

Use the asynchronous `getProperty` and `setProperty` to read and manipulate individual values.
 
```
await tester.setProperty('on', true);
const value = await tester.getProperty('on');
```

### Events
To request an array of recent events, use the asynchronous function `getEvents`.

### Actions
To request an array of recent actions, use the asynchronous function `getActions`.

### Use with Jest
While this package can be use in any NodeJS script, it makes sense to use it in continuous testing. Here is an example how to use it with Jest. The WebThing tested in this example outputs the sum of `a` and `b` in a property `y`.

```
const WebThingHTTPTester = require('webthing-http-tester');

describe('test', () => {
  let tester = new WebThingHTTPTester({port: 8000});

  test('0 + 0 = 0', async () => {
    await tester.setProperty('a', 0);
    await tester.setProperty('b', 0);
    expect(await tester.getProperty('y')).toBe(0);
  });
  test('76 + 34 = 110', async () => {
    await tester.setProperty('a', 76);
    await tester.setProperty('b', 34);
    expect(await tester.getProperty('y')).toBe(110);
  });
});
```
