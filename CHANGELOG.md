# Changelog for Web Thing HTTP Tester

## v0.2.1 (2019-06-20)

- Update README.

## v0.2.0 (2019-01-27)

- Added: Methods to request common links
  - actions
  - events
  - properties

## v0.1.0 (2018-12-29)

- Initial release
